define all_tasks
	# reading from default_pset
	# maybe it should be read from all_psets in future...
	set $default_pset = default_pset
	set $ntask = 0
	set $p_tasks = &default_pset.tasks
	set $p_task = (task_t)($p_tasks->next)
	while $p_tasks != $p_task
		printf "task %d %p \"%s\":\n", $ntask, $p_task, $p_task->name
		printf " thread_count: %d\n", $p_task->thread_count
		set $p_task = (task_t)($p_task->pset_tasks.next)
		set $ntask = $ntask + 1
	end
end

define all_tasks_threads
	set $default_pset = default_pset
	set $ntask = 0
	set $p_tasks = &default_pset.tasks
	set $p_task = (task_t)($p_tasks->next)
	while $p_tasks != $p_task
		printf "task %d %p \"%s\":\n", $ntask, $p_task, $p_task->name
		printf " thread_count: %d\n", $p_task->thread_count
		all_threads_by_task $p_task
		set $p_task = (task_t)($p_task->pset_tasks.next)
		set $ntask = $ntask + 1
	end
end

define all_threads_by_task
	set $p_task = $arg0
	set $nthread = 0
	set $p_threads = &$p_task->thread_list
	set $p_thread = (thread_t)($p_threads->next)
	while $p_threads != $p_thread
		printf " thread %d %p:\n", $nthread, $p_thread
		set $p_thread = (thread_t)($p_thread->thread_list.next)
		set $nthread = $nthread + 1
	end
end
